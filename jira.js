// ==UserScript==
// @name         JIRA customization
// @namespace    http://tampermonkey.net/
// @version      0.5
// @description  try to take over the world!
// @author       You
// @match        https://tornbannerjira.atlassian.net/*
// @require      https://gist.githubusercontent.com/BrockA/2625891/raw/9c97aa67ff9c5d56be34a55ad6c18a314e5eb548/waitForKeyElements.js
// @require      https://cdnjs.cloudflare.com/ajax/libs/jquery/1.7.2/jquery.min.js
// @downloadURL    https://bitbucket.org/signed32/tbs-tampermonkey/raw/master/jira.js
// @updateURL    https://bitbucket.org/signed32/tbs-tampermonkey/raw/master/jira.js
// ==/UserScript==
// @require      https://cdnjs.cloudflare.com/ajax/libs/arrive/2.4.1/arrive.min.js
// @grant        none

(function() {
    'use strict';

    function update_cl_fields(id) {
        var el = $(id).first();
        var cl = el.text().trim();
        var cls = cl.replace(",", "").split(" ");
        var links = cls.map(function(cl) {
            return "<a href=\"http://swarm.tornbannerstudios.local/changes/" + cl + "\">" + cl + "</a>";
        });
        el.html(links.join(" "));
        //console.log(links.join(" "));
    }

    function bind_el(id) {
        waitForKeyElements(id, function () {
            update_cl_fields(id);
        });
    }

    function update_cl_in_comments(id) {
        //console.log(id);
        waitForKeyElements(id, function (node) {
            //console.log(node);
            $(node).map(function(idx, el) {
                //console.log(el);
                var content = $(el).text().replace(/(Change )([0-9]+)( submitted)/, "$1<a href=\"http://swarm.tornbannerstudios.local/changes/$2\">$2</a>$3");
                $(el).html(content);
            });
        });
    }

    bind_el("#customfield_11500-val");
    bind_el("#customfield_11600-val");

    update_cl_in_comments("div[id^='comment-'] .action-body p:contains('Change'):contains('submitted')");
    update_cl_in_comments("div.activity-item-summary div.activity-item-description blockquote p:contains('Change'):contains('submitted')");

//    console.log("#################################################################################################################################################");
})();
